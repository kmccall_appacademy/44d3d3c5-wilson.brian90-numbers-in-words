class Fixnum

  def in_words
    if self == 0
      return "zero"
    end

    num_hash = { 1000000000000 => "trillion", 1000000000 => "billion", 1000000 => "million",
        1000 => "thousand", 100 => "hundred",
        90 => "ninety", 80 => "eighty", 70 => "seventy", 60 => "sixty",
        50 => "fifty", 40 => "forty", 30 => "thirty", 20 => "twenty",
        19 =>"nineteen", 18 =>"eighteen", 17 =>"seventeen", 16 =>"sixteen",
        15 =>"fifteen", 14 =>"fourteen", 13 =>"thirteen", 12 =>"twelve",
        11 => "eleven", 10 => "ten", 9 => "nine", 8 => "eight",
        7 => "seven", 6 => "six", 5 => "five", 4 => "four", 3 => "three",
        2 => "two", 1 => "one"
      }

      return_array = []
      temp = nil

      num_hash.each do |k, v|
        if (self / k) >= 1
          if k >= 100
            return_array << (self/k).in_words
          end
          return_array << v
          temp = self % k
          break
        end
      end

      if temp > 0
        return_array << temp.in_words
      end
      puts return_array

      return_array.flatten.join(" ")

  end
end
